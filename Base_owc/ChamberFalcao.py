import numpy as np
import matplotlib.pylab as plt
from scipy.interpolate import interp1d

class ChamberFalcao:
    
    """Representa la camara del modelo de Falcao"""
    def __init__(self,v0,h,b,a,rw,g,t,dt,nu,dnu,calc=True):
        self.V0=v0
        self.H=h
        self.B=b
        self.A=a
        self.RW=rw
        self.G=g
        self.Nu=nu
        self.DNu=dnu
#    def DiscreteDispertionRelation(self,w,h):
        """Usando metodo de Newton , resolver la relacion de dispersion x*tanh(x*h)=w**2/g encuentra k que resuelve la ecuacion, partiendo de x0=w**2/g"""        
        if calc :            
            self.W=np.linspace(0,self.Nu,int(self.Nu/self.DNu))
            self.K=np.zeros(np.size(self.W))
            for i in range(np.size(self.W)):
                x1=0.1
                k=self.W[i]**2/self.G
                x0=k
                n=10
                for nn in range(n):
                    x0=x1        
                    f0=x0*np.tanh(self.H*x0)-k
                    df0=np.tanh(x0*self.H)+x0*self.H/(np.cosh(x0*self.H)**2)
                    x1=x0-f0/df0
                self.K[i]=x1
            self.DRC=-2*self.W/(1+self.W**2/self.G*self.H/np.sinh(self.K*self.H)**2)*self.B/self.RW/self.G/self.K*np.sin(self.K*self.A)**2   
            self.DispertionRelation=interp1d(self.W,self.K,'linear');
        else: print(" must load a file with precalculated data" )
        
    def AproximatedRadiationCoefficient(self,w):
        k=self.DispertionRelation(w)
        mu=1/(1+w**2/self.G*self.H/np.sinh(k*self.H)**2)
        drc=-2*w*mu*self.B/self.RW/self.G/k*np.sin(k*self.A)**2
        return drc;
    
    def AproximateGeometryKernel(self,T,w):
        bw=self.AproximatedRadiationCoefficient(w)
        dw=w[1]-w[0]
        gr=np.zeros(np.size(T))
        for nn in range(np.size(T)):
            gr[nn]=2/np.pi*np.dot(bw,np.cos(T[nn]*w))*dw
        return gr 

v0=1050.0
h=8.0
b=12.0
a=12.0
Nt=1000.0
t=10.0
dt=t/Nt
Nnu=100000.0
nu=1000.0
dnu=nu/Nnu
g=9.8
rw=1025
# 10.000.000 es el orden de memoria que soporta mi pc
camara=ChamberFalcao(v0,h,b,a,rw,g,t,dt,nu,dnu)